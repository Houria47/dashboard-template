import { useNavigate } from "react-router-dom";
import { Box, Stack, Typography } from "@mui/material";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";

import Submit from "@/components/buttons/Submit";
import PasswordInput from "@/components/inputs/PasswordInput";
import UsernameInput from "@/components/inputs/UsernameInput";
import loginSchema, { loginDefault } from "@/features/login/validation";
import useAxiosErrorSnackbar from "@/hooks/useAxiosErrorSnackbar";

import { accountApis } from "@/API/Account/apis";
import { LoginBody } from "@/API/Account/types";

import { storage } from "@/utils/storage";

const LoginForm = () => {
  const navigate = useNavigate();
  const errorSnackbar = useAxiosErrorSnackbar();

  const {
    control,
    handleSubmit,
    formState: { isSubmitting },
  } = useForm<LoginBody>({
    resolver: yupResolver(loginSchema),
    defaultValues: loginDefault,
  });

  const onSubmit = async (body: LoginBody) => {
    try {
      const token = await accountApis.login(body);
      storage.setToken(token);
      navigate("/", { replace: true });
    } catch (err: unknown) {
      errorSnackbar(err);
    }
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Typography
        component="h2"
        color="primary"
        sx={{
          typography: { sm: "h4", xs: "h6" },
          mx: "auto !important",
          textAlign: "center",
        }}
      >
        تسجيل الدخول
      </Typography>
      <Stack gap={2} mt={5}>
        <UsernameInput control={control} name="userName" />
        <PasswordInput control={control} name="password" />
      </Stack>
      <Box mx="auto" sx={{ my: 2 }} width="fit-content">
        <Submit isSubmitting={isSubmitting}>تسجيل الدخول</Submit>
      </Box>
    </form>
  );
};

export default LoginForm;
