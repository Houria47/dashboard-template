import { LoginBody } from "@/API/Account/types";
import * as yup from "yup";
import { ObjectSchema } from "yup";

export const loginDefault: LoginBody = {
  username: "",
  password: "",
};

export const userNameRegex = /^[a-z0-9_-]*$/i;
export const passwordRegex =
  /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()\-_=+{};:,<.>/?[\]\\|])\S{8,}$/;

const loginSchema: ObjectSchema<LoginBody> = yup.object({
  username: yup
    .string()
    .trim()
    .required("الحقل مطلوب")
    .matches(
      userNameRegex,
      "يجب ان يكون بالانجليزية او ارقام او _ - وبدون فراغات"
    ),
  password: yup
    .string()
    .required("الحقل مطلوب")
    .min(8)
    .matches(
      passwordRegex,
      "يجب أن تحوي على الأقل على حرف كبير، حرف صغير، حرف خاص، رقم"
    ),
});
export default loginSchema;
