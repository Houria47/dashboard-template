import { SettingsCellOutlined } from "@mui/icons-material";
import { ReactNode } from "react";

export type NavLink = {
  linkText: string;
  href: string;
  icon: ReactNode;
  children?: NavLink[];
};

export const navLinks: NavLink[] = [
  {
    linkText: "الإعدادات",
    href: "",
    icon: <SettingsCellOutlined />,
  },
];
