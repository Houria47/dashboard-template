import { InfiniteData } from "@tanstack/react-query";
import { Pagination } from "@/types/apis";
import { AxiosError } from "axios";

export type ReactQueryPage<T> = Pagination<T>;

export function getNextPageParam<T>(
  lastPage: ReactQueryPage<T>,
  allPages: ReactQueryPage<T>[]
) {
  return allPages.length < lastPage.totalPages
    ? lastPage.pageNumber + 1
    : undefined;
}

export function getPreviousPageParam<T>(
  lastPage: ReactQueryPage<T>,
  allPages: ReactQueryPage<T>[]
) {
  return allPages.length > 0 ? lastPage.pageNumber - 1 : undefined;
}

export function getPage<T>(
  data: InfiniteData<Pagination<T>> | undefined,
  pageNumber: number
) {
  return data?.pages[(data?.pageParams[pageNumber] as any) ?? 0]?.data ?? [];
}

export function parseBackendError(err: AxiosError<any, any>) {
  let message: string | undefined;
  const data = err.response?.data;
  if (data?.errorMessage) message = data.errorMessage;
  if (err.code === "ERR_NETWORK")
    message = "خطأ بالشبكة! يرجى التحقق من الإتصال بالإنترنت.";
  return message;
}
