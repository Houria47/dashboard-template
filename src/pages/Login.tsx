import { Box, Paper, Stack } from "@mui/material";

import LoginForm from "@/features/login/LoginForm";

import bg from "@/assets/images/login-bg.jpg";

const Login = () => {
  return (
    <Box width="100vw" height="100vh" overflow="hidden">
      <Box id="LoginBackground">
        <img src={bg} alt="accounting red bg" />
      </Box>
      <Stack
        alignItems="center"
        justifyContent="center"
        height="100%"
        sx={{ p: 2 }}
      >
        <Paper sx={{ py: 4, px: 3, zIndex: 10, minWidth: "min(500px,100%)" }}>
          <LoginForm />
        </Paper>
      </Stack>
    </Box>
  );
};

export default Login;
