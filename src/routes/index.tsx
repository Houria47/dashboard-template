/* eslint-disable react-refresh/only-export-components */
import { lazy } from "react";
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
} from "react-router-dom";

import AppErrorBoundary from "@/components/error-boundaries/AppErrorBoundary";
import AuthenticatedRoute from "@/components/routes/AuthenticatedRoute";
import { WithScroll } from "@/components/routes/WithScroll";
import { SuspensedOutlet } from "@/components/routes/SuspensedOutlet";
import NotAuthenticatedRoute from "@/components/routes/NotAuthenticatedRoute";
import Layout from "@/layout";

const Home = lazy(() => import("@/pages/Home"));
const Login = lazy(() => import("@/pages/Login"));

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route element={<WithScroll />} errorElement={<AppErrorBoundary />}>
      <Route element={<NotAuthenticatedRoute />}>
        <Route element={<SuspensedOutlet />}>
          <Route path="/login" element={<Login />} />
        </Route>
      </Route>
      <Route element={<AuthenticatedRoute />}>
        <Route element={<Layout />}>
          <Route element={<SuspensedOutlet />}>
            <Route index element={<Home />} />
          </Route>
        </Route>
      </Route>
    </Route>
  )
);

export default router;
