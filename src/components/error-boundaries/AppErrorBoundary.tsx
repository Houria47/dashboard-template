import SomethingWentWrong from "@/components/feedback/SomethingWentWrong";
import { Stack, Typography } from "@mui/material";
import { useRouteError } from "react-router-dom";

const AppErrorBoundary = () => {
  const error = useRouteError() as any;
  console.log(error);
  return (
    <Stack justifyContent="center" alignItems="center" height="100vh">
      <SomethingWentWrong />
      <Typography variant="subtitle2" sx={{ color: "#777" }}>
        see console for more details
      </Typography>
    </Stack>
  );
};

export default AppErrorBoundary;
