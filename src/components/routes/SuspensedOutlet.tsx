import { Suspense } from "react";
import { Outlet } from "react-router-dom";

import { TopBarProgress } from "@/components/feedback/TopBarProgress";

export function SuspensedOutlet() {
  return (
    <Suspense fallback={<TopBarProgress />}>
      <Outlet />
    </Suspense>
  );
}
