import { Box, Stack, Typography } from "@mui/material";
import { SxProps } from "@mui/system";
import NoDataIcon from "@/components/icons/NoDataIcon";
type Props = {
  sx?: SxProps;
};
const NoData = ({ sx }: Props) => {
  return (
    <Stack
      alignItems={"stretch"}
      py={1}
      gap={1}
      maxWidth={1}
      width={400}
      mx="auto"
      sx={{ ...sx }}
    >
      <Box sx={{ flex: 1 }}>
        <NoDataIcon />
      </Box>
      <Typography color="primary" variant="h5" textAlign={"center"}>
        لا يوجد بيانات
      </Typography>
    </Stack>
  );
};
export default NoData;
