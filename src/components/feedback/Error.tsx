import { AxiosError } from "axios";
import { StackProps } from "@mui/material";

import ClientError from "./ClientError";
import SomethingWentWrong from "./SomethingWentWrong";

import { parseBackendError } from "@/utils/apiHelpers";
import { Navigate } from "react-router-dom";

export type ErrorProps = { error: unknown; retry?: () => void } & StackProps;

export const Error = ({ error, retry, ...props }: ErrorProps) => {
  let message;
  let status = -1; //
  if (error instanceof AxiosError) {
    message = parseBackendError(error);
    status = error.response?.status ?? status;
    if (error?.code === "ERR_NETWORK")
      message = "خطأ بالشبكة! يرجى التحقق من الإتصال بالإنترنت.";
  }
  if (status == 401) {
    return <Navigate to="/login" />;
  }
  return status === 500 ? (
    <SomethingWentWrong retry={retry} {...props} />
  ) : (
    <ClientError message={message} retry={retry} {...props} />
  );
};
export default Error;
