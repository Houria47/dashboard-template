import {
  CircularProgress,
  CircularProgressProps,
  Stack,
  StackProps,
} from "@mui/material";

type Props = CircularProgressProps & {
  show?: boolean;
  stackProps?: StackProps;
};

const Loading = ({ stackProps, show = true, ...props }: Props) => {
  return show ? (
    <Stack alignItems={"center"} justifyContent="center" {...stackProps}>
      <CircularProgress {...props} />
    </Stack>
  ) : (
    <></>
  );
};
export default Loading;
