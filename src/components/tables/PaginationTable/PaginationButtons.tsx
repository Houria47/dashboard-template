import { TablePagination } from "@mui/material";
import { InfiniteData } from "@tanstack/react-query";
import { Pagination } from "@/types/apis";

interface PaginationTableProps {
  data?: InfiniteData<Pagination<unknown>>;
  page: number;
  handleChangePage: (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => void;
}

const PaginationButtons = ({
  data,
  page,
  handleChangePage,
}: PaginationTableProps) => {
  const isDisabled = !data;

  return (
    <TablePagination
      rowsPerPageOptions={[data?.pages[0].data?.length ?? 0]}
      labelDisplayedRows={({ from, to, count }) =>
        `${from} - ${to} من ${count}`
      }
      component="div"
      count={data?.pages[0].totalDataCount ?? 0}
      rowsPerPage={10}
      page={page}
      onPageChange={handleChangePage}
      SelectProps={{
        disabled: isDisabled,
      }}
    />
  );
};

export default PaginationButtons;
