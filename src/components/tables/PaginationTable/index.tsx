import {
  Box,
  TableBody,
  TableCell,
  TableContainer,
  TableHeadProps,
} from "@mui/material";
import Paper, { PaperProps } from "@mui/material/Paper";
import { Stack } from "@mui/system";
import { InfiniteData, UseInfiniteQueryResult } from "@tanstack/react-query";
import Skeleton, { SkeletonProps } from "@/components/feedback/Skeleton";
import SomethingWentWrong from "@/components/feedback/SomethingWentWrong";
import RepeatELement from "@/components/layout/RepeatElement";
import { ReactElement, ReactNode } from "react";
import { Pagination } from "@/types/apis";
import Loading from "@/components/feedback/Loading";
import NoData from "@/components/feedback/NoData";
import Table from "../TableWithAlign";
import PaginationButtons from "./PaginationButtons";
import TableRowStriped from "./TableRowStriped";
import { useHandlePageChange } from "./useHandlePageChange";
type Props = {
  infiniteQuery: UseInfiniteQueryResult<
    InfiniteData<Pagination<unknown>>,
    unknown
  >;
  children: ReactNode;
  pageNumber: number;
  tableHead: ReactElement<TableHeadProps>;
  customerMessage?: ReactNode;
} & PaperProps &
  (
    | {
        skeleton?: true;
        cellCount: number;
        rowCount?: number;
        skeletonProps?: SkeletonProps;
      }
    | {
        skeleton?: false;
        cellCount?: undefined;
        rowCount?: undefined;
        skeletonProps?: undefined;
      }
  );
const PaginationTable = ({
  infiniteQuery,
  children,
  pageNumber,
  skeleton,
  skeletonProps,
  tableHead,
  cellCount,
  rowCount,
  customerMessage,
  ...props
}: Props) => {
  const {
    fetchNextPage,
    fetchPreviousPage,
    data,
    isInitialLoading,
    isSuccess,
    isError,
  } = infiniteQuery;
  const handlePageChange = useHandlePageChange({
    fetchNextPage,
    fetchPreviousPage,
    pages: data?.pages,
  });

  const isEmpty = isSuccess && !data?.pages[0].data?.length;
  return (
    <Paper
      {...props}
      sx={{ borderRadius: 2, mb: 6, overflow: "hidden", ...props.sx }}
    >
      <Stack>
        <TableContainer>
          <Table>
            {tableHead}
            {isSuccess && children}
            {isInitialLoading && skeleton && (
              <RepeatELement repeat={rowCount ?? 6} container={<TableBody />}>
                <RepeatELement
                  repeat={cellCount}
                  container={<TableRowStriped />}
                >
                  <TableCell>
                    <Skeleton
                      widthRange={{ min: 20, max: 40 }}
                      height={30}
                      sx={{ m: "auto" }}
                      {...skeletonProps}
                    />
                  </TableCell>
                </RepeatELement>
              </RepeatELement>
            )}
          </Table>
        </TableContainer>
        <Box sx={{ mx: "auto", my: 1, width: "min(90% ,300px)" }}>
          {isInitialLoading && !skeleton && <Loading />}
          {isEmpty && <NoData />}
          {isError && <SomethingWentWrong />}
          {customerMessage}
        </Box>
        {data?.pages[0].totalDataCount !== 0 && (
          <PaginationButtons
            page={pageNumber}
            handleChangePage={handlePageChange}
            data={data}
          />
        )}
      </Stack>
    </Paper>
  );
};

export default PaginationTable;
