import { AxiosError } from "axios";
import { useSnackbarContext } from "@/context/snackbar-context";
import { parseBackendError } from "../utils/apiHelpers";
import { Navigate } from "react-router-dom";

const useAxiosErrorSnackbar = () => {
  const snackbar = useSnackbarContext();
  return function (err: unknown) {
    let message = "عذرا حصل أمر ما خاطئ!";
    if (err instanceof AxiosError) {
      if (err.status == 401) {
        return <Navigate to="/login" />;
      }
      message = parseBackendError(err) ?? message;
    }
    snackbar({ message, severity: "error" });
  };
};
export default useAxiosErrorSnackbar;
