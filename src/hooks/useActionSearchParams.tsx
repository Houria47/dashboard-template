import { useSearchParams } from "react-router-dom";

const useActionSearchParams = ({
  idKey = "id",
  addKey = "add",
  editKey = "edit",
  detailsKey = "details",
  removeKey = "remove",
} = {}) => {
  const [searchParams, setSearchParams] = useSearchParams();
  const isActive = [addKey, editKey, detailsKey, removeKey].includes(
    searchParams.get("mode") ?? ""
  );
  const isEdit = searchParams.get("mode") === editKey;
  const isDetails = searchParams.get("mode") === detailsKey;
  const isAdd = searchParams.get("mode") === addKey;
  const isRemove = searchParams.get("mode") === removeKey;
  const id = isEdit || isRemove ? searchParams.get(idKey) : "";
  const clearActionParams = () => {
    searchParams.delete(idKey);
    searchParams.delete("mode");
    setSearchParams(searchParams, { replace: true });
  };
  return {
    id,
    isActive,
    isEdit,
    isDetails,
    isRemove,
    isAdd,
    clearActionParams,
  };
};
export default useActionSearchParams;
