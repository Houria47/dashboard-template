import { ReactNode } from "react";
import createCache from "@emotion/cache";
import { CacheProvider } from "@emotion/react";
import rtlPlugin from "stylis-plugin-rtl";

const cacheRtl = createCache({
  key: "muirtl",
  stylisPlugins: [rtlPlugin],
});

type Props = { children: ReactNode };

const DirectionProvider = ({ children }: Props) => {
  return <CacheProvider value={cacheRtl}>{children}</CacheProvider>;
};
export default DirectionProvider;
