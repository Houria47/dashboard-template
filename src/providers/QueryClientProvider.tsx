import { ReactNode } from "react";
import { QueryClientProvider as Provider } from "@tanstack/react-query";

import { queryClient } from "@/context/queryClient";

type Props = { children: ReactNode };

const QueryClientProvider = ({ children }: Props) => {
  return <Provider client={queryClient}>{children}</Provider>;
};
export default QueryClientProvider;
