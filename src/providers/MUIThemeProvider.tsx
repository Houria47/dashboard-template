import { ReactNode } from "react";
import { ThemeProvider } from "@mui/material";

import theme from "@/context/theme-context";

type Props = { children: ReactNode };

const MUIThemeProvider = ({ children }: Props) => {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

export default MUIThemeProvider;
