import { ReactNode } from "react";
import { CssBaseline } from "@mui/material";

import QueryClientProvider from "@/providers/QueryClientProvider";
import MUIThemeProvider from "@/providers/MUIThemeProvider";
import DirectionProvider from "@/providers/DirectionProvider";
import SnackbarProvider from "@/providers/SnackbarProvider";

type Props = { children: ReactNode };

const Providers = ({ children }: Props) => {
  return (
    <MUIThemeProvider>
      <QueryClientProvider>
        <SnackbarProvider>
          <DirectionProvider>
            <CssBaseline />
            {children}
          </DirectionProvider>
        </SnackbarProvider>
      </QueryClientProvider>
    </MUIThemeProvider>
  );
};

export default Providers;
