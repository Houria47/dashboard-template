import { AlertSeverity } from "@/components/feedback/Snackbar";
import { ReactNode, createContext, useContext } from "react";

export type SnackbarProps = {
  severity: AlertSeverity;
  message: ReactNode;
};

export type SnackBarContextValue = {
  setSnackbarProps: ({ severity, message }: SnackbarProps) => void;
  handleOpenSnackbar: () => void;
};

export const initialSnackbarState = {
  setSnackbarProps: () => {},
  handleOpenSnackbar: () => {},
};

export const SnackbarContext =
  createContext<SnackBarContextValue>(initialSnackbarState);

export const useSnackbarContext = () => {
  const { setSnackbarProps, handleOpenSnackbar } = useContext(SnackbarContext);
  const showSnackbar = (props: SnackbarProps) => {
    setSnackbarProps(props);
    handleOpenSnackbar();
  };
  return showSnackbar;
};
