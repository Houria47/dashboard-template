import { createTheme } from "@mui/material";
import themeConstants from "@/constants/theme";

const theme = createTheme({
  direction: "rtl",
  typography: {
    fontFamily: "MontserratArabic",
    allVariants: {
      color: themeConstants.foreground,
    },
  },
  palette: {
    primary: {
      main: themeConstants.primary,
      "50": themeConstants.primary05,
      "900": themeConstants.primary9,
    },
    secondary: {
      main: themeConstants.secondary,
    },
    background: { default: themeConstants.background },
    action: { hover: themeConstants.primary05 },
    info: { main: themeConstants.info },
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        "::-webkit-scrollbar": {
          width: 6,
          height: 6,
        },
        "::-webkit-scrollbar-track": {
          background: "background",
        },
        "::-webkit-scrollbar-thumb": {
          background: themeConstants.primary,
          borderRadius: "6px",
        },
        "::-webkit-scrollbar-thumb:hover": {
          background: themeConstants.primary9,
        },
      },
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          "& label": {
            color: themeConstants.secondary,
          },
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          borderRadius: themeConstants.inputBorderRadius,
          "& fieldset": {
            borderColor: themeConstants.primary,
          },
          "&:hover fieldset": {
            borderColor: themeConstants.primary,
          },
          "&.Mui-focused fieldset": {
            borderColor: themeConstants.primary,
          },
        },
      },
    },
    MuiTableHead: {
      styleOverrides: {
        root: {
          backgroundColor: themeConstants.primary,
          ".MuiTableCell-root": {
            color: "white",
          },
        },
      },
    },
  },
});
theme.shadows[1] = `0 0 5px RGBA( 73, 113, 116, 0.3)`;

export default theme;
