import API_ROUTES from "@/constants/api-routes";
import { LoginBody } from "@/API/Account/types";
import axios from "@/libs/axios";

export const accountApis = {
  login: async (body: LoginBody) => {
    const { data } = await axios.post(`${API_ROUTES.ACCOUNT.LOGIN}`, body);
    return data;
  },
};
