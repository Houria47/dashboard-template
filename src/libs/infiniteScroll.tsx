import { ReactNode, useEffect, useRef } from "react";
import { UseInfiniteQueryResult } from "@tanstack/react-query";
import Infinite from "react-infinite-scroll-component";

import { Stack, StackProps } from "@mui/material";

import Loading from "@/components/feedback/Loading";
import { Pagination } from "@/types/apis";

type Props = {
  loader?: ReactNode;
  children: ReactNode;
  query: UseInfiniteQueryResult<Pagination<unknown>, unknown>;
} & StackProps;

const InfiniteScroll = ({ query, children, loader, ...props }: Props) => {
  const { fetchNextPage, hasNextPage, data, isSuccess } = query;
  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (window.innerHeight > (ref.current?.scrollHeight ?? 0)) {
      query.fetchNextPage();
    }
  }, [query]);

  return isSuccess ? (
    <Infinite
      style={{ overflow: "unset" }}
      dataLength={data.totalDataCount}
      next={fetchNextPage}
      hasMore={hasNextPage ?? false}
      loader={loader ?? <Loading size={25} />}
    >
      <Stack ref={ref} {...props} component={"div"}>
        {children}
      </Stack>
    </Infinite>
  ) : (
    <></>
  );
};
export default InfiniteScroll;
