import { useState } from "react";
import { Outlet } from "react-router-dom";
import { Box, Toolbar, useMediaQuery, useTheme } from "@mui/material";

import { AppBar } from "@/layout/AppBar";
import Main from "./Main";
import Sidebar from "@/layout/Sidebar";

export const drawerWidth = 250;

const Layout = () => {
  const theme = useTheme();
  const isLargeScreen = useMediaQuery(theme.breakpoints.up("md"));

  const [open, setOpen] = useState(isLargeScreen);
  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };
  return (
    <Box sx={{ height: "100vh" }}>
      <AppBar
        open={open}
        onDrawerOpen={handleDrawerOpen}
        onDrawerClose={handleDrawerClose}
      />
      <Sidebar open={open} setOpen={setOpen} />
      <Toolbar />
      <Main
        open={open}
        sx={{ px: { xs: 1, sm: 5 }, py: 3, minHeight: "100svh" }}
      >
        <Outlet />
      </Main>
    </Box>
  );
};
export default Layout;
