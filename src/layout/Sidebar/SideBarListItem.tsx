import {
  Collapse,
  List,
  ListItemIcon,
  SxProps,
  Typography,
} from "@mui/material";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import { Stack } from "@mui/system";
import { NavLink } from "@/constants/nav-links";
import { Fragment } from "react";
import { useLocation } from "react-router-dom";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import OptionalLink from "@/components/links/OptionalLink";
export type ActiveItem = [active: boolean, href: string];

const LinkSx: SxProps = {
  display: "block",
  color: "#000",
  "&:hover": { color: "primary.main" },
  "& .MuiListItemButton-root": {
    "&.Mui-selected": {
      backgroundColor: "primary.100",
    },
  },
};
export type SideBarListItemProps = {
  data: NavLink;
  sideBarIsOpen: boolean;
  activeItem: ActiveItem;
  setActiveItem: React.Dispatch<React.SetStateAction<[boolean, string]>>;
  level: number;
  onClick: (haveChildren: boolean) => void;
};
export const SideBarListItem = ({
  data,
  sideBarIsOpen,
  activeItem,
  onClick,
  level,
  setActiveItem,
}: SideBarListItemProps) => {
  const pathname = useLocation().pathname;
  const isActive = activeItem[1] === data.href && activeItem[0];
  const isOpen = isActive || childActive(activeItem[1], data.children);

  const handleClick = () => {
    setActiveItem([!isOpen, data.href]);
    onClick(!!data.children);
  };

  return (
    <Fragment key={data.href}>
      <OptionalLink
        withLink={!data.children}
        sx={{
          textDecoration: "none !important",
          color: "#000",
          "&:hover": { color: "primary" },
          backgroundColor: !data.children ? "red" : "yellow",
        }}
        href={data.href}
      >
        <ListItem
          key={data.href}
          disablePadding
          sx={{ ...LinkSx, backgroundColor: level > 0 ? "#f4f4f4" : "#fff6" }}
        >
          <ListItemButton
            selected={
              (pathname.startsWith(data.href) && data.href !== "") ||
              pathname === data.href
            }
            className="fade"
            sx={{
              minHeight: 48,
              justifyContent: sideBarIsOpen ? "initial" : "center",
              px: 2.5,
            }}
            onClick={handleClick}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                mr: { xs: 1, sm: sideBarIsOpen ? 3 : 0 },
                justifyContent: "center",
              }}
            >
              {data.icon}
            </ListItemIcon>
            <ListItemText>
              <Stack
                direction={"row"}
                justifyContent="space-between"
                sx={{
                  opacity: sideBarIsOpen ? 1 : 0,
                  ...(!data.href && {
                    color: "primary.main",
                    fontWeight: "550",
                  }),
                }}
              >
                <Typography>{data.linkText}</Typography>
                {data.children && (isOpen ? <ExpandLess /> : <ExpandMore />)}
              </Stack>
            </ListItemText>
          </ListItemButton>
        </ListItem>
      </OptionalLink>
      <Collapse in={isOpen}>
        <List component="div" disablePadding>
          {data.children?.map((sideBarItem) => (
            <SideBarListItem
              onClick={onClick}
              level={level + 1}
              key={sideBarItem.href}
              activeItem={activeItem}
              setActiveItem={setActiveItem}
              data={sideBarItem}
              sideBarIsOpen={sideBarIsOpen}
            />
          ))}
        </List>
      </Collapse>
    </Fragment>
  );
};

function childActive(activeHref: string, children: NavLink[] | undefined = []) {
  return children.flat(Infinity).some((item) => item.href === activeHref);
}

export default SideBarListItem;
