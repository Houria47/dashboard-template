import { useLocation, useNavigate } from "react-router-dom";
import LogoutIcon from "@mui/icons-material/Logout";
import MenuIcon from "@mui/icons-material/Menu";
import { Tooltip } from "@mui/material";
import MuiAppBar, { AppBarProps as MuiAppBarProps } from "@mui/material/AppBar";
import IconButton from "@mui/material/IconButton";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { styled } from "@mui/material/styles";

import { storage } from "@/utils/storage";
import { drawerWidth } from "@/layout";
import { navLinks } from "@/constants/nav-links";

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

function getTabName(href: string) {
  let tabName = "الإعدادات";
  if (href) {
    const link = navLinks.find((link) => link.href === `/${href}`);
    if (link) tabName = link.linkText;
  }
  return tabName;
}

export const AppBarStyled = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})<AppBarProps>(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));
type Props = {
  open: boolean;
  onDrawerOpen: () => void;
  onDrawerClose: () => void;
};
export const AppBar = ({ open, onDrawerOpen, onDrawerClose }: Props) => {
  const pageHref = useLocation().pathname.split("/")[1];
  const navigate = useNavigate();

  const handleLogout = async () => {
    storage.clearToken();
    // if token not exist, the user already not logged in -> redirect to login page.
    if (!storage.getToken()) {
      navigate("/login");
      return;
    }
  };
  return (
    <AppBarStyled position="fixed" open={open}>
      <Toolbar>
        <IconButton
          color="inherit"
          onClick={open ? onDrawerClose : onDrawerOpen}
          edge="start"
        >
          <MenuIcon sx={{ color: "white" }} />
        </IconButton>
        <Typography
          variant="h6"
          sx={{ width: "100%" }}
          color="white"
          noWrap
          component="div"
        >
          {getTabName(pageHref)}
        </Typography>

        <Tooltip title="تسجيل الخروج">
          <IconButton onClick={handleLogout}>
            <LogoutIcon sx={{ color: "white" }} />
          </IconButton>
        </Tooltip>
      </Toolbar>
    </AppBarStyled>
  );
};
